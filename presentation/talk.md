---
title: Astro
date: 10.10.2022
---

---

## Astro

- Multi-Page-Application Web-framework
- Eingesetzt von u.A. Google, Netlify, Trivago
- Seit August in 1.0
- Für Content-fokussierte Webseiten
- Server-Side-Generation wo möglich, um statisches HTML auszuliefern
- Content-Islands für Interaktivität
- Neu: Unterstützt auch Server-Side-Rendering

---

## Bestandteile

- Astro Components
- Bring Your Own UI Framework (BYOF) - Komponenten aus:
  - React
  - Preact
  - Svelte
  - Vue
  - Solid
  - AlpineJS
  - Lit
- Pages
- Layouts
- Statische oder Dynamische (SSR) Endpunkte

---

## Astro Component

- Grundbaustein eines Astro-Projekts
- `.astro`-Dateiendung
- Reine HTML Template-Komponenten
- Ohne clientseitige Runtime
- Können Script enthalten, wird zur Bauzeit (bzw. bei Request falls SSR) ausgeführt
- Unterstützen `props` (+ Typescript Type-Checking) und Slots per `<slot />`
- Syntax stark an JSX angelehnt

---

## Astro Component vs. JSX

- Curly-Braces für Variablen-Interpolation & dynamisches HTML
- Trennung von Template und Script per Markdown-Fence (`---`)
- `kebab-case` statt `camelCase` für Attribute
- HTML-Kommentare statt Javascript-Kommentaren im Template

```{.javascript data-filename="src/components/example.astro"}
const name = "World";
const items = ["Up", "Down", "Strange", "Charm", "Truth", "Beauty"]

---

<h1 title={name}>Hello, {name}!</h1>
<!-- Ein Kommentar -->
<ul>
  {items.map((item) => (
      <li>{item}</li>
  ))}
</ul>
```

---

## Pages

- Dateien im `src/pages/`-Verzeichnis
- Verantwortlich für Routing, Laden der Daten und Seitenlayout
- File-Based Routing -> Jede Datei in `src/pages/` ist ein Endpunkt, abhängig vom Dateipfad
- Verlinkung zwischen Seiten per `<a>`-Element

---

## Astro Pages

- `.astro` - Dateiendung
- Gleiches Featureset wie Astro Components

```{.javascript data-filename="src/pages/index.astro"}
---
---
<html lang="en">
  <head>
    <title>My Homepage</title>
  </head>
  <body>
    <h1>Welcome to my website!</h1>
  </body>
</html>
```

---

## Astro Pages mit Layout

- Auslagerung in Layouts erspart Wiederholungen

```{.javascript data-filename="src/pages/index.astro"}
---
import MySiteLayout from '../layouts/MySiteLayout.astro';
---
<MySiteLayout>
  <p>My page content, wrapped in a layout!</p>
</MySiteLayout>
```

---

## Markdown (& MDX) Pages

- `.md` - Dateien
- Mit MDX-Integration: Auch `.mdx` - Dateien
- Layouts per `layout` front-matter Eigenschaft

```{data-filename="src/pages/myMarkdownPage.md"}
---
layout: '../layouts/MySiteLayout.astro'
title: 'My Markdown page'
---

# Title

This is my page, written in **Markdown.**
```

---

## HTML-Seiten

- `.html`-Dateien
- Ohne Features aus Astro-Seiten

---

## Layout

- Sonderform einer Astro-Komponente
- Für wiederverwendbare Page-Templates nützlich
- Stellt eine Seitenhülle (`<html>`, `<head>` & `<body>`) und einen `<slot>` zur Verfügung

```{.html data-filename="src/layouts/MySiteLayout.astro"}
---
---
<html lang="en">
  <head>
    <title>Astro</title>
  </head>
  <body>
    <nav>
      <a href="#">Home</a>
      <a href="#">Posts</a>
    </nav>
    <article>
      <slot /> <!-- Hier wird der Content injiziert -->
    </article>
  </body>
</html>
```

---

## Routing

- Dynamische oder statische Pages
- Dynamische Pages per `src/pages/beispiel/[name].astro`
- `getStaticPaths()` deklariert, welche dynamischen Pfade möglich sind
- Per SSR: Dynamische Pages beim Request

---

## SSR

- Experimenteller Support seit Frühjahr 2022
- Deployment per Adapter bei verschiedenen Anbietern:
  - Cloudflare
  - Deno Deploy
  - Vercel
  - Netlify
- Oder in Node per exportiertem `handler`:
  - per `http` oder `https` - Modul
  - In einem Middleware-Framework wie Express
- Serverseitige scripts bei Request aufgerufen, nicht zur Bauzeit
- `request` - Objekt steht zur Verfügung

---

## Content Islands / Islands Architecture

<blockquote cite="https://www.patterns.dev/posts/islands-architecture/">
The islands architecture encourages small, focused chunks of interactivity within server-rendered web pages. The output of islands is progressively enhanced HTML, with more specificity around how the enhancement occurs. Rather than a single application being in control of full-page rendering, there are multiple entry points. The script for these "islands" of interactivity can be delivered and hydrated independently, allowing the rest of the page to be just static HTML.</blockquote>

<center>(<a href="https://www.patterns.dev/posts/islands-architecture/">patterns.dev</a>)</center>

- Serverseitiges HTML
- Progressive Enhancement
- Kleine, interaktive Regionen

---

## Islands Architecture vs. Micro-Frontends

- Ansatz sehr ähnlich
- Bei Content-Islands: Komposition über HTML, zusammen mit statischen Teilen

---

## Islands Architecture & Progressive Hydration

- Islands Architecture ermöglicht eine Form der Progressive Hydration
- Progressive Hydration
  - Teile einer Application werden nach und nach hydriert
- Islands Architecture:
  - Kein Top-Down Rendering, da kein "root"-Element
  
---

## SSR, Progressive Hydration & Islands Architecture

![Vergleich zwischen SSR, Progressive Hydration & Islands Architecture (Quelle: <a href="https://www.patterns.dev/posts/progressive-hydration/">patterns.dev</a>)](images/progressive-hydration.png)

---

## Content Islands in Astro

- Komponenten aus React, Preact, Vue, Svelte, ... werden importiert
- Standardmässig werden diese Komponenten zur Bauzeit/Serverseitig gerendert

```js
import MyReactComponent from '../components/MyReactComponent.jsx';

---

<MyReactComponent />
```

- Interaktivität per `client` Directive
```js
import MyReactComponent from '../components/MyReactComponent.jsx';

---

<MyReactComponent client:load />
```

---

## Client-Directives

- `client:load`
  - Lädt sofort
- `client:idle`
  - Lädt nach `requestIdleCallback`
- `client:visible`
  - Lädt, sofern im Viewport sichtbar
- `client:media={mediaQuery}`
  - Lädt bei entsprechender CSS media query
- `client:only={framework}`
  - Funktioniert wie `client:load`, nur ohne vorherige SSG
  - Zusätzlich muss das Framework angegeben werden, z.B.:

```js
<MyReactComponent client:only="react" />
```

---

## Beispiel

---

## Integrations

- Offizielle Integrations:
  - UI Frameworks
  - SSR Adapter
  - Andere (image Optimierung, Tailwind, etc.)
- 100+ Community Integrations

---

## Links

- [Progressive Hydration (patterns.dev)](https://www.patterns.dev/posts/progressive-hydration/)
- [Islands Architecture (patterns.dev)](https://www.patterns.dev/posts/islands-architecture/)
- [Blog-Post zur Islands-Architecture (jasonformat.com)](https://jasonformat.com/islands-architecture/)
- [Astro Docs (docs.astro.build)](https://docs.astro.build/en/getting-started/)
- [Slides](https://tiborpilz.gitlab.io/presentation-astro/#/title-slide)
- [Notizen/Artikel](https://gitlab.com/tiborpilz/presentation-astro/-/blob/main/notes.org)

---

## Fin

- Fragen?
