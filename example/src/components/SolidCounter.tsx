/** @jsxImportSource solid-js */

import { createSignal } from 'solid-js';

/** A counter written with Solid */
export default function SolidCounter({ children }) {
	const [count, setCount] = createSignal(0);
	const add = () => setCount(count() + 1);
	const subtract = () => setCount(count() - 1);

	return (
		<section>
			<h2>{children}</h2>
			<div id="solid" class="counter">
				<button onClick={subtract}>-</button>
				<pre>{count()}</pre>
				<button onClick={add}>+</button>
			</div>
		</section>
	);
}
