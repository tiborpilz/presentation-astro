/** @jsxImportSource preact */

import { useState } from 'preact/hooks';

/** A counter written with Preact */
export function PreactCounter({ children }) {
	const [count, setCount] = useState(0);
	const add = () => setCount((i) => i + 1);
	const subtract = () => setCount((i) => i - 1);

	return (
		<section>
			<h2>{children}</h2>
			<div class="counter">
				<button class="btn" onClick={subtract}>-</button>
				<pre>{count}</pre>
				<button class="btn" onClick={add}>+</button>
			</div>
		</section>
	);
}
