import { defineConfig } from 'astro/config';
import preact from '@astrojs/preact';
import react from '@astrojs/react';
import svelte from '@astrojs/svelte';
import vue from '@astrojs/vue';
import solid from '@astrojs/solid-js';
import tailwind from "@astrojs/tailwind";

export default defineConfig({
  integrations: [preact(), react(), svelte(), vue(), solid(), tailwind()],
  base: '/presentation-astro/example'
});
